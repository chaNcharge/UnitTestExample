package main.java;

public class Main {

    public static void main(String[] args) {
        Main m = new Main();
        System.out.println("Hello world!");
        System.out.println(m.divide(10, 2));
        System.out.println(m.isDivisible(4, 2));
    }

    public double divide(double dividend, double divisor) {
        if (divisor == 0) {
            return -1;
        } else {
            return dividend / divisor;
        }
    }

    public boolean isDivisible(int dividend, int divisor) {
        return divisor != 0 && dividend / divisor == (double)dividend / divisor;
    }
}
