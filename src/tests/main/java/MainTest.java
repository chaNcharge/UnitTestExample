package main.java;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    private Main t = new Main();

    @Test
    void divideTest() {
        assertEquals((double)-1, t.divide(1337, 0));
        assertEquals(2, t.divide(4, 2));
    }

    @Test
    void isDivisibleTest() {
        assertFalse(t.isDivisible(1337, 0));
        assertFalse(t.isDivisible(2, 3));
        assertTrue(t.isDivisible(4, 2));
    }
}